const randomize = require('randomatic')
const got = require('got')
const fs = require('fs')
const Queue = require('bull')

const queue = new Queue('watergun-queue')

function generateFromPattern(template) {
	const mapping = {'数字':'0', '字母': 'a'}
	const tagRegex = /\{([^\^\-}]+)\-?(\d+)?\}/g
	return template.replace(tagRegex, (match, tag, size)=>{
		if (!mapping[tag]) return '{'+tag+'}'
		return randomize(mapping[tag], size?size:3)
	})
}

function generateUrls(template, domain, size) {
	template = template.replace('{域名}', 'http://'+domain)
	return [...Array(size)]
		.map(x => generateFromPattern(template))
}

function push(domain, token, urls) {
    const api = `http://data.zz.baidu.com/urls?site=${domain}&token=${token}`
    return got.post(api, {
        body: urls.join('\n'),
        headers: {
			'Content-Type': 'text/plain'
        },
        timeout: 5000
    })
}

const template = '{域名}/web{字母-5}/{数字-6}.html'

queue.process(1, async (job)=>{
    const {domain, token} = job.data
    const urls = generateUrls(template, domain, 1000)
    const res = await push(domain, token ,urls)
    return res.body
})