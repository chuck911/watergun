const Queue = require('bull')
const samples = require('lodash.samplesize')
const icaili = require('./icaili.json')

const queue = new Queue('watergun-queue')

;(async ()=>{
	for (const domain in icaili) {
		const data = icaili[domain]
		const token = data.token
		const subs = samples(data.domains, 10)
		subs.push('www')
		for (const sub of subs) {
			const subdomain = sub+'.'+domain
			console.log(subdomain, token)
			await queue.add({domain:subdomain, name: subdomain, token}, {attempts: 2, backoff: 10000})
		}
	}
	process.exit()
})()

