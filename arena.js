const arena = require('bull-arena')

arena({
    host: "127.0.0.1",
    queues: [
        {
            name: 'watergun-queue',
            "hostId": "local",
        }
    ],
    // basePath: '/q',
})